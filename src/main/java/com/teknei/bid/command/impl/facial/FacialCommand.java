package com.teknei.bid.command.impl.facial;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.persistence.entities.BidClieRegEsta;
import com.teknei.bid.persistence.entities.BidEstaProc;
import com.teknei.bid.persistence.entities.BidScan;
import com.teknei.bid.persistence.entities.BidClieTas;
import com.teknei.bid.persistence.repository.BidClieRegEstaRepository;
import com.teknei.bid.persistence.repository.BidEstaProcRepository;
import com.teknei.bid.persistence.repository.BidScanRepository;
import com.teknei.bid.persistence.repository.BidTasRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public class FacialCommand implements Command {

    @Autowired
    @Qualifier(value = "parseFacialCommand")
    private Command parseFacialCommand;
    @Autowired
    @Qualifier(value = "storeTasFacialCommand")
    private Command storeTasFacialCommand;
    @Autowired
    @Qualifier(value = "persistFacialCommand")
    private Command persistFacialCommand;
    @Autowired
    private BidScanRepository bidScanRepository;
    @Autowired
    private BidTasRepository bidTasRepository;
    @Autowired
    @Qualifier(value = "statusCommand")
    private Command statusCommand;
    @Autowired
    private BidClieRegEstaRepository regEstaRepository;
    @Autowired
    private BidEstaProcRepository bidEstaProcRepository;
    @Autowired
    private BidClieRegEstaRepository bidClieRegEstaRepository;
    private static final String ESTA_PROC = "CAP-FAC";
    private static final Logger log = LoggerFactory.getLogger(FacialCommand.class);

    @Override
    public CommandResponse execute(CommandRequest request) {
    	//log.info("lblancas:: [tkn-service-facial] :: "+this.getClass().getName()+".execute");
        String scanId = findScanId(request.getId());
        String documentId = findDocumentId(request.getId());
        request.setDocumentId(documentId);
        request.setScanId(scanId);
        CommandResponse parseResponse = parseFacialCommand.execute(request);
        if (parseResponse.getStatus().equals(Status.FACIAL_MBSS_ERROR)) {
            String id = parseResponse.getDesc();
            parseResponse.setStatus(Status.FACIAL_ERROR);
            parseResponse.setDesc(String.valueOf(Status.FACIAL_MBSS_ERROR.getValue()));
            parseResponse.setId(request.getId());
            saveStatus(request.getId(), Status.FACIAL_MBSS_ERROR, request.getUsername());
            return parseResponse;
        }
        saveStatus(request.getId(), Status.FACIAL_MBSS_OK, request.getUsername());
        CommandResponse tasResponse = storeTasFacialCommand.execute(request);
        if (tasResponse.getStatus().equals(Status.FACIAL_TAS_ERROR)) {
            tasResponse.setStatus(Status.FACIAL_ERROR);
            tasResponse.setDesc(String.valueOf(Status.FACIAL_TAS_ERROR.getValue()));
            saveStatus(request.getId(), Status.FACIAL_TAS_ERROR, request.getUsername());
            return tasResponse;
        }
        saveStatus(request.getId(), Status.FACIAL_TAS_OK, request.getUsername());
        CommandRequest dbRequest = new CommandRequest();
        dbRequest.setData(tasResponse.getDesc());
        dbRequest.setScanId(request.getScanId());
        dbRequest.setDocumentId(request.getDocumentId());
        dbRequest.setId(request.getId());
        dbRequest.setUsername(request.getUsername());
        CommandResponse dbResponse = persistFacialCommand.execute(dbRequest);
        if (dbResponse.getStatus().equals(Status.FACIAL_DB_ERROR)) {
            dbResponse.setDesc(String.valueOf(Status.FACIAL_DB_ERROR.getValue()));
            dbResponse.setStatus(Status.FACIAL_ERROR);
            saveStatus(request.getId(), Status.FACIAL_DB_ERROR, request.getUsername());
        } else {
            dbResponse.setDesc(String.valueOf(Status.FACIAL_DB_OK.getValue()));
            dbResponse.setStatus(Status.FACIAL_OK);
            saveStatus(request.getId(), Status.FACIAL_DB_OK, request.getUsername());
            updateStatus(request.getId(), request.getUsername());
        }
        return dbResponse;
    }


    /**
     * Persists the current status for the main process
     *
     * @param id
     * @param status
     * @return
     */
    private CommandResponse saveStatus(Long id, Status status, String username) {
    	//log.info("lblancas:: [tkn-service-facial] :: "+this.getClass().getName()+".saveStatus id:"+id +" username["+username+"] status["+status+"]") ;
        CommandRequest request = new CommandRequest();
        request.setId(id);
        request.setRequestStatus(status);
        request.setUsername(username);
        CommandResponse response = statusCommand.execute(request);
        return response;
    }

    private String findScanId(Long id) {
    	//log.info("lblancas:: [tkn-service-facial] :: "+this.getClass().getName()+".findScanId id:"+id);
        try {
            BidScan responseScan = bidScanRepository.findByIdRegi(id);
            if (responseScan == null) {
                return null;
            }
            return responseScan.getScanId();
        } catch (Exception e) {
            log.error("Error find scanId: {}", e.getMessage());
        }
        return null;
    }

    private String findDocumentId(Long id) {
    	//log.info("lblancas:: [tkn-service-facial] :: "+this.getClass().getName()+".findDocumentId id:"+id);
        try {
            BidClieTas responseScan = bidTasRepository.findByIdClie(id);
            if (responseScan == null) {
                return null;
            }
            return responseScan.getIdTas();
        } catch (Exception e) {
            log.error("Error find scanId: {}", e.getMessage());
        }
        return null;
    }

    private void updateStatus(Long idClient, String username) {
    	//log.info("lblancas:: [tkn-service-facial] :: "+this.getClass().getName()+".updateStatus idClient["+idClient+"] username["+username+"]");
        try {
            BidEstaProc estaProc = bidEstaProcRepository.findTopByCodEstaProcAndIdEsta(ESTA_PROC, 1);
            BidClieRegEsta regEsta = bidClieRegEstaRepository.findByIdClieAndIdEstaProc(idClient, estaProc.getIdEstaProc());
            if (regEsta == null) {
                log.warn("Status for process: {} found null", idClient);
            }
            regEsta.setEstaConf(true);
            regEsta.setFchModi(new Timestamp(System.currentTimeMillis()));
            regEsta.setUsrModi(username);
            regEsta.setUsrOpeModi(username);
            bidClieRegEstaRepository.save(regEsta);
        } catch (Exception e) {
            log.error("Error finding status of process for customer: {} with message: {}", idClient, e.getMessage());
        }
    }
}
