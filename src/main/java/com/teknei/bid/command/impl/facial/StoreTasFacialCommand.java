package com.teknei.bid.command.impl.facial;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.dto.PersonData;
import com.teknei.bid.persistence.entities.BidScan;
import com.teknei.bid.persistence.repository.BidScanRepository;
import com.teknei.bid.persistence.repository.BidTasRepository;
import com.teknei.bid.util.tas.TasManager;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@Component
public class StoreTasFacialCommand implements Command {
	private static final Logger log = LoggerFactory.getLogger(StoreTasFacialCommand.class);
    @Value("${tkn.tas.name}")
    private String tasName;
    @Value("${tkn.tas.surname}")
    private String tasSurname;
    @Value("${tkn.tas.lastname}")
    private String tasLastname;
    @Value("${tkn.tas.identification}")
    private String tasIdentification;
    @Value("${tkn.tas.date}")
    private String tasDate;
    @Value("${tkn.tas.scan}")
    private String tasScan;
    @Value("${tkn.tas.id}")
    private String tasOperationId;
    @Value("${tkn.tas.casefile}")
    private String tasCasefile;
    @Value("${tkn.tas.idType}")
    private String tasTypeId;
    @Value("${tkn.tas.face}")
    private String tasFace;
    @Autowired
    private TasManager tasManager;
    @Autowired
    private BidScanRepository bidScanRepository;
    @Autowired
    private BidTasRepository bidTasRepository;

    @Override
    public CommandResponse execute(CommandRequest request) 
    {
    	//log.info("lblancas:: [tkn-service-facial] :: "+this.getClass().getName()+".execute ");
        CommandResponse commandResponse = new CommandResponse();
        try {
            Long id = request.getId();
            String scanId = bidScanRepository.findByIdRegi(id).getScanId();
            String tasId = bidTasRepository.findByIdClie(id).getIdTas();
            request.setDocumentId(tasId);
            request.setScanId(scanId);
            commandResponse.setScanId(scanId);
            commandResponse.setDocumentId(tasId);
            JSONObject jsonResponse = addPhoto(request.getFileContent().get(0), request.getScanId(), request.getDocumentId(), request.getId());
            if (jsonResponse.getBoolean("created")) {
                commandResponse.setStatus(Status.FACIAL_TAS_OK);
            } else {
                throw new IllegalArgumentException();
            }
        } catch (Exception e) {
            commandResponse.setStatus(Status.FACIAL_TAS_ERROR);
        }
        return commandResponse;
    }


    public JSONObject addPhoto(byte[] content, String scanId, String idDocumentManager, Long operationId) throws Exception 
    {
    	//log.info("lblancas:: [tkn-service-facial] :: "+this.getClass().getName()+".addPhoto ");
        Map<String, String> docProperties = getMetadataMapAddress(scanId, operationId);
        String docNumber = docProperties.get(tasIdentification);
        JSONObject response = tasManager.addDocument(tasFace, idDocumentManager, null, docProperties, content, "image/jpeg", "Rostro-" + docNumber + ".jpeg");
        return response;
    }

    private Map<String, String> getMetadataMapAddress(String scanId, Long operationId) throws Exception 
    {
    	//log.info("lblancas:: [tkn-service-facial] :: "+this.getClass().getName()+".getMetadataMapAddress ");
        PersonData scanInfo = getPersonalDataFromScan(scanId, operationId);
        Map<String, String> docProperties = new HashMap<>();
        docProperties.put(tasName, scanInfo.getName());
        docProperties.put(tasSurname, scanInfo.getSurename());
        docProperties.put(tasLastname, scanInfo.getSurenameLast());
        docProperties.put(tasIdentification, scanInfo.getPersonalNumber());
        String dateISO8601 = ZonedDateTime.now().format(DateTimeFormatter.ISO_INSTANT).toString();
        docProperties.put(tasDate, dateISO8601);
        return docProperties;
    }


    public PersonData getPersonalDataFromScan(String scanId, Long operationId) throws Exception 
    {
    	//log.info("lblancas:: [tkn-service-facial] :: "+this.getClass().getName()+".getPersonalDataFromScan ");
        JSONObject internalDocument;
        PersonData personalData = new PersonData();
        String icarInfo = null;
        BidScan scan = bidScanRepository.findByScanId(scanId);
        icarInfo = bidScanRepository.findByScanId(scanId).getData();
        JSONObject icarJsonInfo = new JSONObject(icarInfo);
        internalDocument = icarJsonInfo.getJSONObject("document");
        personalData.setPersonalNumber(String.valueOf(operationId));
        personalData.setName(internalDocument.getString("name"));
        personalData.setSurename(internalDocument.optString("firstSurname", ""));
        personalData.setSurenameLast(internalDocument.optString("secondSurname", ""));
        personalData.setLastNames(internalDocument.optString("surname", ""));
        return personalData;
    }

}
